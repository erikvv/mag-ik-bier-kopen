
import React, { useEffect, useState } from 'react';
import './App.css';

function App() {
    return (
        <>
            <h1 className="App-header">Mag ik bier kopen?</h1>
            <div className="App-questionnaire">
                <Questionnaire />
            </div>
            <footer>
                <a href="https://stemlp.nl">
                    <img src="/lp.jpg" className="lp-logo" />
                </a>
                <div class="right-footer">
                    <a href="https://gitlab.com/erikvv/mag-ik-bier-kopen" class="gitlab-link">
                        <img src="/gitlab.svg" className="gitlab-logo" />
                        Code
                    </a>
                    <a href="https://betaalverzoek.rabobank.nl/betaalverzoek/?id=vYT0dPlaTn6egQzcx-EVvg" class="ideal-link">
                        <img src="/iDeal.svg" className="iDeal-logo" />
                        Doneer
                    </a>
                </div>
            </footer>
        </>
    );
}

function Questionnaire() {
    const [state, setState] = useState('start');
    useEffect(() => {
        window.history.pushState(state, state);
    }, [])
    const [time, setTime] = useState(getCurrentTime());

    window.onpopstate = event => {
        if (event.state) {
            setState(event.state);
        }
    }

    const transition = newState => {
        window.history.pushState(newState, newState);
        setState(newState);
    }

    if (state === 'start') {
        return (
            <button className="App-button" onClick={() => transition('country')}>Start</button>
        );
    }

    if (state === 'country') {
        return (
            <>
                <p>Waar woon je?</p>
                <button className="App-button" onClick={() => transition('time')}>Nederland</button>
                <button className="App-button" onClick={() => transition('wat-denk-je-zelf')}>Saudi-Arabië</button>
            </>
        );
    }

    if (state === 'wat-denk-je-zelf') {
        return (
            <p>Wat denk je zelf?</p>
        )
    }

    if (state === 'JA') {
        return <>
            <p className="result">JA</p>
        </>
    }

    if (state === 'NEE') {
        return (
            <p className="result">NEE</p>
        )
    }

    if (state === 'ja-snel') {
        return (
            <>
                <p className="result">JA</p>
                <p>Je moet wel snel zijn</p>
            </>
        )
    }

    if (state === 'nu-vertrekken') {
        return <>
            <div>Je moet</div>
            <div class="result">NU</div>
            <div>vertrekken</div>
        </>
    }

    if (state === 'te-laat') {
        return (
            <p>Tenzij je nu bij de kassa staat, ben je te laat</p>
        )
    }

    if (state === 'bijna-weer') {
        return (
            <p>Goed volgehouden, het mag bijna weer</p>
        )
    }
    
    if (state === 'time') {
        const onClickVerder = () => {
            const nextState = determineTimeState(time);
            transition(nextState);
        }

        return (
            <>
                <p>Hoe laat is het?</p>
                <TimeInput time={time} setTime={setTime} />
                <button className="App-button" onClick={onClickVerder}>
                    Verder
                </button>
            </>
        );
    }
}

function TimeInput({ time, setTime }) {
    return (
        <input className="time-input" type="time" value={time} onChange={ev => setTime(ev.target.value)} />
    )
}

function getCurrentTime() {
    const now = new Date();
    const hours = now.getHours().toString().padStart(2, '0')
    const minutes = now.getMinutes().toString().padStart(2, '0')
    const formValue = `${hours}:${minutes}`;
    return formValue;
}

function timeToPair(time) {
    const parts = time.split(':');

    return {
        hours: parts[0],
        minutes: parts[1],
    }
}

function determineTimeState(time) {
    const pair = timeToPair(time);

    if (pair.hours == 19 && pair.minutes >= 40 && pair.minutes <= 50) {
        return 'ja-snel';
    }

    if (pair.hours == 19 && pair.minutes >= 50 && pair.minutes <= 54) {
        return 'nu-vertrekken';
    }

    if (pair.hours == 19 && pair.minutes >= 55) {
        return 'te-laat';
    }

    if (pair.hours == 5) {
        return 'bijna-weer';
    }

    if (
        (pair.hours >= 6 && pair.hours < 19)
        || 
        (pair.hours == 19 && pair.minutes < 40)
    ) {
        return 'JA';
    }

    return 'NEE';
}

export default App;
