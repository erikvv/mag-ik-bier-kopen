# Mag ik bier kopen?

[www.magikbierkopen.nl](http://www.magikbierkopen.nl/)

Gebouwd met [Create React App](https://github.com/facebook/create-react-app). 

Suggestie? Open een issue.

Uw donaties worden uitsluitend besteed aan bier:

[![iDeal](./public/iDeal.svg)](https://betaalverzoek.rabobank.nl/betaalverzoek/?id=vYT0dPlaTn6egQzcx-EVvg)

[![PayPal](./paypal.gif)](https://www.paypal.com/donate?hosted_button_id=W6D3JFS4RJDAJ)
